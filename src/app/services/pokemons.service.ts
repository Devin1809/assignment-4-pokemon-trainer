import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Pokemon } from './models/pokemon.model';

@Injectable({
    providedIn: 'root'
})
export class PokemonsService{
    
    private _pokemons: Pokemon;
    private _error: string = '';
    private _image: string = '';

    constructor(private readonly http: HttpClient) {
    }

    public fetchPokemons(): void {
        this.http.get<Pokemon>('https://pokeapi.co/api/v2/pokemon?limit=24')
            .subscribe((pokemons: Pokemon) => {
                this._pokemons = pokemons;
            }, (error: HttpErrorResponse) => {
                this._error = error.message;
            });
    }

    public fetchNewPokemons(url: string): void {
        this.http.get<Pokemon>(url)
            .subscribe((pokemons: Pokemon) => {
                this._pokemons = pokemons;
            }, (error: HttpErrorResponse) => {
                this._error = error.message;
            });
    }

    public pokemons(): Pokemon {
        return this._pokemons;
    }

    public error(): string {
        return this._error;
    }

}