import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class DetailsService{
    
    private _details: any;
    private _error: string = '';

    constructor(private readonly http: HttpClient) {
    }

    public fetchDetails(nr: string): void {
        this.http.get<any>(`https://pokeapi.co/api/v2/pokemon/${nr}`)
            .subscribe((details: any) => {
                this._details = details;
            }, (error: HttpErrorResponse) => {
                this._error = error.message;
            });
    }

    public details(): any {
        return this._details;
    }

    public error(): string {
        return this._error;
    }

}