import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';

import { LoginPage } from './pages/login/login.page';
import { TrainerPage } from './pages/trainer/trainer.page';
import { CataloguePage } from './pages/catalogue/catalogue.page';
import { DetailsPage } from './pages/details/details.page';

import { LoginInputComponent } from './components/login-input/login-input.component';
import { LoginBootbackComponent } from './components/login-bootback/login-bootback.component';
import { CollectedListComponent } from './components/collected-list/collected-list.component';
import { PokemonListComponent } from './components/pokemon-list/pokemon-list.component';
import { DetailsDisplayComponent } from './components/details-display/details-display.component';


import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';


@NgModule({
  declarations: [
    AppComponent,
    LoginPage,
    TrainerPage,
    CataloguePage,
    DetailsPage,
    LoginInputComponent,
    LoginBootbackComponent,
    CollectedListComponent,
    PokemonListComponent,
    DetailsDisplayComponent 
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
