import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';

import { LoginPage } from './pages/login/login.page';
import { TrainerPage } from './pages/trainer/trainer.page';
import { CataloguePage } from './pages/catalogue/catalogue.page';
import { DetailsPage } from './pages/details/details.page';

const routes: Routes= [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: '/login'
    },
    {
        path: 'login',
        component: LoginPage
    },
    {
        path: 'trainer',
        component: TrainerPage
    },
    {
        path: 'catalogue',
        component: CataloguePage
    },
    {
        path: 'details',
        component: DetailsPage
    }
]

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {
    
}