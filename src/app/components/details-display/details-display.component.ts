import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DetailsService } from 'src/app/services/details.service';

@Component({
    selector: 'app-details-display',
    templateUrl: './details-display.component.html',
    styleUrls: ['./details-display.component.css']
})

export class DetailsDisplayComponent implements OnInit {

    constructor(private readonly detailService: DetailsService, private router: Router) { 

    }

    detailsNumber: string = localStorage.getItem("detailsNumber");
    detailsImage: string = localStorage.getItem("detailsImage");
    detailsName: string = localStorage.getItem("detailsName");
    pokemonCollected: [string] = JSON.parse(localStorage.getItem("collectedMons"));
    imagesCollected: [string] = JSON.parse(localStorage.getItem("collectedImages"));
    namesCollected: [string] = JSON.parse(localStorage.getItem("collectedNames"));

    ngOnInit() {
        this.detailService.fetchDetails(this.detailsNumber);
    }

    get details(): any {
        return this.detailService.details();
    }

    public collectMon() {
        this.collectionHandler(this.detailsNumber, this.pokemonCollected, "collectedMons");
        this.collectionHandler(this.detailsImage, this.imagesCollected, "collectedImages");
        this.collectionHandler(this.detailsName, this.namesCollected, "collectedNames");
        this.router.navigate(['/trainer']);
    }

    public collectionHandler(nr, ar, localName) {
        switch(ar) {
            case null:
                ar = [''];
                ar.push(nr);
                ar.shift();
                break;
            case undefined:
                ar = [''];
                ar.push(nr);
                ar.shift();
                break;
            default:
                ar.push(nr);
                break;
        }
        localStorage.setItem(localName, JSON.stringify(ar));
        console.log(ar);
    }

}