import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
    selector: 'app-login-input',
    templateUrl: './login-input.component.html',
    styleUrls: ['./login-input.component.css']
})

export class LoginInputComponent implements OnInit {

    constructor(private router: Router) { }

    trainerName: string = localStorage.getItem("trainerName");

    ngOnInit() {
        switch(this.trainerName) {
            case null:
                break;
            case '':
                break;
            case undefined:
                break;
            default:
                this.router.navigate(['/catalogue']);
                break;
        }
    }

     public loginSubmit(createForm: NgForm): void {
        switch(createForm.value.trainerName) {
            case null:
                break;
            case '':
                break;
            case undefined:
                break;
            default:
                localStorage.setItem("trainerName", createForm.value.trainerName);
                this.router.navigate(['/catalogue']);
                break;
        }
    }

}