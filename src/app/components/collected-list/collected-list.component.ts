import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-collected-list',
    templateUrl: './collected-list.component.html',
    styleUrls: ['./collected-list.component.css']
})

export class CollectedListComponent implements OnInit {

    constructor(private router: Router) { }

    pokemonCollected: [string] = JSON.parse(localStorage.getItem("collectedMons"));
    imagesCollected: [string] = JSON.parse(localStorage.getItem("collectedImages"));
    namesCollected: [string] = JSON.parse(localStorage.getItem("collectedNames"));
    ngOnInit() {
        
    }

    protected getIdAndImage(url: string): any {
        const id = url.split( '/' ).filter( Boolean ).pop();
        return { id, image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${ id }.png`};
      }

    protected getDetailPage(id: string, image: string, pokemonName: string) {
        localStorage.setItem("detailsNumber", id);
        localStorage.setItem("detailsImage", image);
        localStorage.setItem("detailsName", pokemonName);
        console.log(id);
        console.log(image);
        console.log(pokemonName);
        this.router.navigate(['/details']);
    }

    counter(i: number) {
        return new Array(i);
    }

}