import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router';
import { Pokemon } from 'src/app/services/models/pokemon.model';
import { PokemonsService } from 'src/app/services/pokemons.service';

@Component({
    selector: 'app-pokemon-list',
    templateUrl: './pokemon-list.component.html',
    styleUrls: ['./pokemon-list.component.css']
})
export class PokemonListComponent implements OnInit{

    constructor(private readonly pokemonService: PokemonsService, private router: Router) {
    }
    trainerName: string = localStorage.getItem("trainerName");

    ngOnInit(): void {
        this.pokemonService.fetchPokemons();
    }

    get pokemons(): Pokemon {
        return this.pokemonService.pokemons();
    }

    protected getIdAndImage(url: string): any {
        const id = url.split( '/' ).filter( Boolean ).pop();
        return { id, image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${ id }.png`};
      }

    protected getDetailPage(info: string, pokemonName: string) {
        const pokeNumber = this.getIdAndImage(info);
        localStorage.setItem("detailsNumber", pokeNumber.id);
        localStorage.setItem("detailsImage", pokeNumber.image);
        localStorage.setItem("detailsName", pokemonName);
        console.log(pokeNumber);
        console.log(pokemonName);
        this.router.navigate(['/details']);
    }

    getNewFetch(path: string) {
        this.pokemonService.fetchNewPokemons(path);
        return this.pokemonService.pokemons();
    }

}