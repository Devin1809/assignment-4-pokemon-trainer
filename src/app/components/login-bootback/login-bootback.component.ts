import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router';

@Component({
    selector: 'app-login-bootback',
    templateUrl: './login-bootback.component.html',
    styleUrls: ['./login-bootback.component.css']
})

export class LoginBootbackComponent implements OnInit {

    constructor(private router: Router) { }

    trainerName: string = localStorage.getItem("trainerName");

    ngOnInit() {
        switch(this.trainerName) {
            case null:
                this.router.navigate(['/login']);
                break;
            case '':
                this.router.navigate(['/login']);
                break;
            case undefined:
                this.router.navigate(['/login']);
                break;
            default:
                break;
        }
    }

}